#!/usr/bin/env python

'''
Created on Apr 1, 2015

@author: Shayegan Omidshafiei
'''
import rospy
import rospkg
import numpy as np
from GenericVeh import GenericVeh
from acl_msgs.msg import QuadWaypointQueueComplete, QuadWaypoint, LocalizerTrigger
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Point
import udputil
import time

class MissionManager(object):
	def __init__(self, vehs, vehShouldUseCamera, host):
		self.host = host
		self.port = 21569 
		self.udp_sender = udputil.UdpSender(self.host,self.port)

		self.nVehs = np.size(vehs)
		self.vehs = []
		self.n_major_wpts_so_far = 0
		self.missionComplete = False
		self.vizShouldUpdate = True

		# Waypoints at either end of the room
		self.n_max_major_wpts = 5 # Each major waypoint involves going across the room
		self.wpts_major_obj_seeked = [['bicycle',1], ['tv',2]] # class+number of unique instances of objects seeked at major wpt 1 and wpt 2

		# XYZ and heading angle for observations
		self.wpts_major = np.asarray([ [-0.9, -4.0, 0.8, float(-85)/180*np.pi],
									   [-0.9, 0.5, 0.8, float(90)/180*np.pi]])
		# Deviations to major waypoints (XYZ and heading in rads), where the quad runs the classifier/localizer
		# WAYPOINTS HAVE TO BE FURTHER APART THAN RADIUS SETTING OF TIP!!!!
		self.wpts_minor_delta = np.asarray([ [0.0, 0.0, 0.0, 0.0],
											[2.0, 0.0, 0.0, 0.0]])
		self.initVehs(vehs, vehShouldUseCamera)

		self.rosPubWptLocations = rospy.Publisher('~mission_manager_viz', MarkerArray, queue_size=10)
		self.rosPubLocalizerTrigger = rospy.Publisher('~localizer_trigger', LocalizerTrigger, queue_size=10)
		self.rosSubCommandComplete = rospy.Subscriber('/waypoint_manager/wpt_queue_complete', QuadWaypointQueueComplete, self.callbackCommandComplete)
	
	def tmaTrigger(self, event=None):
		# mm logic:
		#   - takeoff
		#   - repeat K times:
		#     - go to next major waypoint region
		#     - while object not found:
		#           - go to next mini waypoint
		#           - do a surveillance trajectory (simply yawing maybe) until you get n consecutive hits on object of interest
		#     - if object is confirmed, publish some UDP messages to acl_projector to visualize something interesting
		#     - else, do nothing and continue to next major waypoint
		#   - land

		if (self.n_major_wpts_so_far < self.n_max_major_wpts):
			for veh in self.vehs:
				if (veh.latest_wpt_complete):
					print '~~~~~~~~~  ',100.0*self.n_major_wpts_so_far/(float(self.n_max_major_wpts)),'percent complete! ~~~~'
					self.vizShouldUpdate = True
					
					# Observations only conducted at minor wpts
					if veh.new_wpt_is_minor:
						classObsDict = veh.getClassObsDict() # string of all high-probability class observations in viewport
						print 'i_major_wpt', veh.i_major_wpt, '| obs', classObsDict, 'received'
						
						obj_seeked_class = self.wpts_major_obj_seeked[veh.i_major_wpt][0]
						obj_seeked_count = self.wpts_major_obj_seeked[veh.i_major_wpt][1]
						if (not classObsDict) or (obj_seeked_class not in classObsDict.keys()) or (obj_seeked_class in classObsDict.keys() and classObsDict[obj_seeked_class] != obj_seeked_count):
							# Either no objects were seen, or object of interest was not seen, or wrong count of object of interest was seen
							# Should go to next minor wpt if one exists (otherwise goes to next major wpt automatically)
							veh.setWptToNextMinor()
							# Publish a "didn't see any object of interest" msg for acl_projector
							self.pubLocalizerTrigger(str_found_obj = "")
						elif classObsDict[obj_seeked_class] == obj_seeked_count:
							# High confidence classifier and correct # of object of interest seen, go to next major wpt
							veh.setWptToNextMajor()
							self.pubLocalizerTrigger(str_found_obj = obj_seeked_class)
					else:
						# Last wpt was major, so start conducting minor wpts
						self.n_major_wpts_so_far += 1
						veh.setWptToNextMinor()
					
					# Send TMA as waypoints to rvo_path_planner (ROS msg)
					self.publishWpt(veh)

					# New task just started
					veh.latest_wpt_complete = False
					
		else:
			self.missionComplete = True
			print 'Mission complete! All quads should land now!'

	def pubLocalizerTrigger(self, str_found_obj):
		msg = LocalizerTrigger()
		msg.header.stamp = rospy.Time.now()
		msg.class_string = str_found_obj
		msg.trigger_fov = True
		self.rosPubLocalizerTrigger.publish(msg)
		# Makes sure acl_projector shows the fov of the detected objects for a finite amount of time
		rospy.sleep(2.)
		msg.trigger_fov = False
		self.rosPubLocalizerTrigger.publish(msg)

	def sendVehsToFirstBase(self):
		print 'Sending takeoff and move msg to all vehicles'
		for veh in self.vehs:
			veh.sendTakeoffCmd()
			veh.setWptToNextMajor()
			self.publishWpt(veh)
			veh.latest_wpt_complete = False

	def landAllVehs(self):
		print '\n\nSending land msg to all vehicles'
		for veh in self.vehs:
			veh.sendLandNowCmd()
			veh.latest_wpt_complete = False
	
	def initVehs(self, vehs, vehShouldUseCamera):
		for idxVeh in xrange(0, self.nVehs):
			self.vehs.append(GenericVeh(idxVeh, vehs[idxVeh], vehShouldUseCamera[idxVeh], self.wpts_major, self.wpts_minor_delta, self.wpts_major_obj_seeked))
		
	def publishVizMsgs(self, event=None):
		# Publishes wpt locations and object of interest name at each wpt
		if self.vizShouldUpdate:
			msgMarkerArray = MarkerArray()
			wptCoordList = list()

			for i_wpt_major in xrange(0,len(self.wpts_major)):
				for i_wpt_minor in xrange(0,len(self.wpts_minor_delta)):
					# Wpt locations
					msgWptLocations = Marker()
					msgWptLocations.header.stamp = rospy.Time.now()
					msgWptLocations.header.frame_id = "/world"
					msgWptLocations.ns = "rvo_belief_node"
					msgWptLocations.id = i_wpt_major*(1+len(self.wpts_major))+i_wpt_minor
					msgWptLocations.type = Marker.CYLINDER
					msgWptLocations.action = Marker.ADD
					msgWptLocations.pose.position.x = self.wpts_major[i_wpt_major][0]+self.wpts_minor_delta[i_wpt_minor][0]
					msgWptLocations.pose.position.y = self.wpts_major[i_wpt_major][1]+self.wpts_minor_delta[i_wpt_minor][1]
					msgWptLocations.pose.position.z = 0
					msgWptLocations.scale.x = 1.0
					msgWptLocations.scale.y = 1.0
					msgWptLocations.scale.z = 0.01
					msgWptLocations.color.r = 0.0
					msgWptLocations.color.g = 0.8
					msgWptLocations.color.b = 0.0
					msgWptLocations.color.a = 0.7
					msgWptLocations.lifetime = rospy.Duration.from_sec(0)
					msgWptLocations.frame_locked = 0

					wptCoordList.append([self.wpts_major[i_wpt_major][0]+self.wpts_minor_delta[i_wpt_minor][0],
										 self.wpts_major[i_wpt_major][1]+self.wpts_minor_delta[i_wpt_minor][1]])

					# Object of interest names
					msgWptObjOfInterest = Marker()
					msgWptObjOfInterest.header.stamp = rospy.Time.now()
					msgWptObjOfInterest.header.frame_id = "/world"
					msgWptObjOfInterest.ns = "rviz_text"
					msgWptObjOfInterest.id = i_wpt_major*(1+len(self.wpts_major))+i_wpt_minor
					msgWptObjOfInterest.type = Marker.TEXT_VIEW_FACING
					msgWptObjOfInterest.action = Marker.ADD
					msgWptObjOfInterest.pose.position.x = self.wpts_major[i_wpt_major][0]+self.wpts_minor_delta[i_wpt_minor][0]
					msgWptObjOfInterest.pose.position.y = self.wpts_major[i_wpt_major][1]+self.wpts_minor_delta[i_wpt_minor][1]
					msgWptObjOfInterest.pose.position.z = 0.1
					msgWptObjOfInterest.scale.x = 0.2
					msgWptObjOfInterest.scale.y = 0.2
					msgWptObjOfInterest.scale.z = 0.2
					msgWptObjOfInterest.color.r = 1.0
					msgWptObjOfInterest.color.g = 1.0
					msgWptObjOfInterest.color.b = 1.0
					msgWptObjOfInterest.color.a = 1.0
					msgWptObjOfInterest.lifetime = rospy.Duration.from_sec(0)
					msgWptObjOfInterest.frame_locked = 0
					msgWptObjOfInterest.text = 'Seek ' + str(self.wpts_major_obj_seeked[i_wpt_major][1]) + ' ' + self.wpts_major_obj_seeked[i_wpt_major][0] + 's'

					msgMarkerArray.markers.append(msgWptLocations)
					msgMarkerArray.markers.append(msgWptObjOfInterest)

			self.rosPubWptLocations.publish(msgMarkerArray)

			self.udp_sender.send('rvo_belief_node', np.array(wptCoordList), time.time())

	def publishWpt(self, veh):
		rospy.loginfo('[missionmanager][publishWpt][%s] veh will now go to [%d,%d,%d]' %(veh.name,veh.wpt_new[0],veh.wpt_new[1],veh.wpt_new[2]))
		tempWpt = QuadWaypoint()
		tempWpt.point.x = veh.wpt_new[0]
		tempWpt.point.y = veh.wpt_new[1]
		tempWpt.point.z = veh.wpt_new[2]
		tempWpt.heading = veh.wpt_new[3]
		veh.sendWptAddCmd(tempWpt)
			
	def callbackCommandComplete(self, msg):
		print 'Wpt completion msg received for', msg.veh_name
		for veh in self.vehs:
			if ((veh.name == msg.veh_name) and (msg.is_queue_complete) and not self.missionComplete):
				veh.latest_wpt_complete = True
				print 'Wpt completed for ', veh.name

if __name__ == '__main__':
	rospy.init_node('MissionManagerRosNode', anonymous=True)

	host = rospy.get_param("~host")
	vehs = rospy.get_param("~vehs").replace(" ", "").split(',')
	vehShouldUseCamera = rospy.get_param("~vehShouldUseCamera").replace(" ", "").split(',')

	rospack = rospkg.RosPack()
	rosPkgPath = rospack.get_path('boeing_2017')+'/scripts'
	
	mm = MissionManager(vehs = vehs, vehShouldUseCamera = vehShouldUseCamera, host = host)

	# Sleeping req'd due to ROS threading issues
	publishHz = 30
	rospy.sleep(1)
	vizTimer = rospy.Timer(rospy.Duration.from_sec(1.0/publishHz), mm.publishVizMsgs)
	mm.sendVehsToFirstBase()
	mmTimer = rospy.Timer(rospy.Duration.from_sec(0.3), mm.tmaTrigger)
	
	while not mm.missionComplete:
		pass

	vizTimer.shutdown()
	mmTimer.shutdown()
	
	rospy.sleep(1)
	mm.landAllVehs()

	rospy.spin()
