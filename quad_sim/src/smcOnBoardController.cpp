/*
 * OnBoardController.cpp
 *
 *  Created on: Jun 8, 2017
 *      Author: brett
 */

#include "OnBoardController.h"

OnBoardController::OnBoardController()
{

	controlDT = 0.001;
	setGains();

	srand(time(NULL));
}

OnBoardController::~OnBoardController()
{

}

/*---------------------------------------------------------------------
 Function Name: Controller_Update
 Description:   Run the attitude controller, should execute at 1KHz
 Set motor values
 Inputs:        None
 Returns:       None
 -----------------------------------------------------------------------*/
Eigen::Vector4d OnBoardController::runController(Eigen::Quaterniond att,
		Eigen::Vector3d rate, Eigen::Quaterniond att_cmd,
		Eigen::Vector3d rate_cmd, double throttle, int AttCmd)
{

	Eigen::Matrix3d J = Eigen::Matrix3d::Zero();

	J(0,0) = 0.1; J(1,1) = 0.1; J(2,2) = 0.1;
	double l = 0.152;
	double c = 0.14;
	double lam = 100;
	double K = 50;
	double b = 0.035;
	Eigen::Vector3d phi(10,10,10);


	// Quaternion attitude error
	Eigen::Quaterniond qerror = att_cmd.inverse()*att;

	double qe0 = qerror.w();
	double sign=1;

	if (qe0<0) sign=-1;

	Eigen::Vector3d werror(rate-rate_cmd);

	Eigen::Vector3d s(werror+lam*sign*qerror.vec());

	Eigen::Quaterniond Wa;
	Eigen::Quaterniond Wd;

	Wa.w() = 0;
	Wa.vec() << rate;
	Wd.w() = 0;
	Wd.vec() << rate_cmd;

	Eigen::Vector3d d_qe;

	d_qe = 0.5*sign*((qerror*Wa).vec() - (Wd*qerror).vec());

	s(0) = std::min(phi(0),std::max(s(0),-phi(0)));
	s(1) = std::min(phi(1),std::max(s(1),-phi(1)));
	s(2) = std::min(phi(2),std::max(s(2),-phi(2)));

	Eigen::Vector3d Mb(-J*lam*d_qe + rate.cross(J*rate) - J*K*s);

	double li = 1/(4*l);
	double ci = 1/(4*c);

	// Form motor commands
	double m1 =  li*Mb(0) - li*Mb(1) + ci*Mb(2);
	double m2 =  li*Mb(0) + li*Mb(1) - ci*Mb(2);
	double m3 = -li*Mb(0) + li*Mb(1) + ci*Mb(2);
	double m4 = -li*Mb(0) - li*Mb(1) - ci*Mb(2);

	Eigen::Vector4d motorcmd = Eigen::Vector4d::Zero();
	if (1 == AttCmd)
	{
		motorcmd(0) = b*m1 + throttle;
		motorcmd(1) = b*m2 + throttle;
		motorcmd(2) = b*m3 + throttle;
		motorcmd(3) = b*m4 + throttle;
	}
	return motorcmd;
}

void OnBoardController::setGains(void)
{
	// 
	Kp_roll = (625.0)/6250;
	Ki_roll = (0.0)/6250;
	Kd_roll = (1200.5)/6250;

	Kp_pitch = (625.0)/6250;
	Ki_pitch = (0.0)/6250;
	Kd_pitch = (1200.5)/6250;

	Kp_yaw = (2000.0)/6250;
	Ki_yaw = (0.0)/6250;
	Kd_yaw = (500.0)/6250;

	IntRoll = (0.0)/6250;
	IntPitch = (0.0)/6250;
	IntYaw = (0.0)/6250;
	controlDT = (1.0 / 1000.0);
}



