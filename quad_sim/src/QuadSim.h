/*
 * QuadSim.h
 *
 *  Created on: Jun 11, 2014
 *      Author: mark
 */

#ifndef QUADSIM_H_
#define QUADSIM_H_

// ROS includes
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include <tf/transform_broadcaster.h>

// local includes
#include "acl_msgs/QuadCntrl.h"
#include "acl_msgs/ViconState.h"
#include "acl_msgs/QuadAHRS.h"
#include "acl_msgs/FloatStamped.h"
#include "QuadFT.h"
#include "OnBoardController.h"

// ACL Utils Library
//#include "acl/QuadDynamics.hpp"
#include "QuadDynamics.hpp"
#include "acl_utils/utils.hpp"

// const double DT = 1 / 1000.0; ///< default simulation time step
// const double BROADCAST_DT = 1 / 100.0; ///< frame rate of state broadcasting for service mode
const double BROADCAST_TIMEOUT = 2.0; ///< seconds since last call after which state is no longer broadcast

enum CMD_TYPES
{
	NOT_FLYING, ATTITUDE, POSITION, RESET
};

/// command struct
struct sQuadCommand{
    Eigen::Quaterniond Q; ///< Attitude quaternion
    Eigen::Vector3d rate; ///< Body-frame rate
    double throttle;
    int AttCmd;
};

/**
 *  Simple wrapper class for running QuadDynamics in a ros node
 */
class QuadSim
{
public:
	QuadSim(const std::string& name, double dt);
	virtual ~QuadSim();

	ros::Publisher pose_pub, twist_pub, state_pub, ahrs_pub, wind_pub;
	acl::QuadDynamics dynamics;
	ros::Timer controller_timer, broadcast_timer;

	void setParams();
	void runSim(const ros::TimerEvent& e);
	void broadcastTimeout(const ros::WallTimerEvent& e);
	void cmdCB(const acl_msgs::QuadCntrl& cmd);
	void broadcastState(const ros::TimerEvent& e);
	acl::sQuadState getInitialState(void);

private:
	OnBoardController obc;
	QuadFT qft;
	acl::sQuadState state;
	sQuadCommand cmd;
  tf::TransformBroadcaster transform_broadcaster_;
  std::string name_;
  double dt_;

};

#endif /* QUADSIM_H_ */
