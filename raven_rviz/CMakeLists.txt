cmake_minimum_required(VERSION 2.8.3)
project(raven_rviz)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  visualization_msgs
  interactive_markers
  acl_msgs
)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES comm_test
  CATKIN_DEPENDS roscpp visualization_msgs interactive_markers
#  DEPENDS system_lib
)



## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS} 
)


## Declare a cpp executable
add_executable(rv src/vehicle_broadcaster.cpp src/agent_marker.cpp src/waypoint.cpp)
add_executable(rw src/worldBroadcaster.cpp)
add_executable(vp src/vehicle_parser.cpp)
#add_executable(wp src/waypoint_main.cpp src/waypoint.cpp)

 target_link_libraries(rv
   ${catkin_LIBRARIES}
 )
   target_link_libraries(rw
   ${catkin_LIBRARIES}
 )
   target_link_libraries(vp
   ${catkin_LIBRARIES}
 )

add_dependencies(rv ${catkin_EXPORTED_TARGETS})
add_dependencies(rw ${catkin_EXPORTED_TARGETS})
add_dependencies(vp ${catkin_EXPORTED_TARGETS})

