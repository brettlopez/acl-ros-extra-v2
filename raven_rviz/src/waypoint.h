/*
 * waypoint.h
 *
 *  Created on: Jun 7, 2012
 *      Author: mark
 */

#ifndef WAYPOINT_H_
#define WAYPOINT_H_

#include <ros/ros.h>
#include <interactive_markers/interactive_marker_server.h>
#include <interactive_markers/menu_handler.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/tf.h>

#include <acl_msgs/Waypoint.h>

using namespace visualization_msgs;

class Waypoint {
public:
	Waypoint(std::string name,
		interactive_markers::InteractiveMarkerServer * server);
	~Waypoint();

	ros::Publisher goal_pub;
private:
	interactive_markers::InteractiveMarkerServer * server;
	interactive_markers::MenuHandler menu_handler;
	interactive_markers::MenuHandler::EntryHandle vel_last, mode_last;

	geometry_msgs::Pose marker_pose;
	geometry_msgs::Pose veh_pose;
	acl_msgs::Waypoint waypoint_msg;
	ros::Subscriber pose_sub;

	std::vector<double> vel_disc;
	std::string name;

	int flight_status;

	double velocity;

	void createWP();
	void eraseWP();
	Marker makeBox( InteractiveMarker &msg, double color[4] );
	void makeWaypointMarker(std::string name);
	void waypointCB( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback );
	void flightModeCB( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback );
	void velocityCB( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback );
	geometry_msgs::Pose project2room(geometry_msgs::Pose pose);
	void poseCB(const geometry_msgs::PoseStamped& msg);
};

#endif /* WAYPOINT_H_ */
