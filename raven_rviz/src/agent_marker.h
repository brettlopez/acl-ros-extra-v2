/*
 * agent_marker.h
 *
 *  Created on: Sep 26, 2013
 *      Author: Mark Cutler
 *		Email:  markjcutler@gmail.com
 */

#ifndef AGENT_MARKER_H_
#define AGENT_MARKER_H_

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PoseStamped.h>
#include <acl_msgs/ViconState.h>
#include <tf/transform_broadcaster.h>

#define MAXTIMEOUT 3.0 // max time we can go without receiving new pose data

class AgentMarker
{
public:
	AgentMarker(std::string name, int id);
	virtual ~AgentMarker();

	// simple retrieve function to get the current marker
	visualization_msgs::Marker getMarker();
	visualization_msgs::Marker getTextMarker();
	bool shouldBroadcast();

private:
	visualization_msgs::Marker markerVehicle; // RAVEN marker - physical location of object in building 41 RAVEN flight space
	visualization_msgs::Marker markerText;				// RAVEN text - display name of vehicle

	void initMarker();
	void viconCallback(const acl_msgs::ViconState &msg);
	void getParamsFromServer(std::string nameType);
	void param(std::string n, float &out);
	void param(std::string n, double &out);
	void param(std::string n, bool &out);
	void param(std::string n, std::string &out);

	geometry_msgs::Vector3 scale; // displayed scale of the vehicle
	std_msgs::ColorRGBA color;
	std::string mesh_resource;
	std::string name;
	bool use_embedded_materials;
	int markerID;
	double roll_offset, pitch_offset, yaw_offset;
	double x_offset, y_offset, z_offset;

	ros::Subscriber sub_pose;
};

#endif /* AGENT_MARKER_H_ */
