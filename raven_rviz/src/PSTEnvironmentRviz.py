#!/usr/bin/env python

import rospy
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point

class RavenBroadcast:
 
    def __init__(self):
        self.x = 0.0
        self.pub = rospy.Publisher("RAVEN_world", MarkerArray)
        self.markerArray = MarkerArray()
        self.setEnvironment()
        
        rospy.Timer(rospy.Duration(.1),self.drawEnvironment)

    def setEnvironment(self):        
        
        #Simulated Wall - Left
        m = Marker(type=Marker.CUBE, action=Marker.ADD)
        m.header.frame_id = 'world'
        m.id = 0
        m.ns = 'wall'
        m.lifetime = rospy.Duration(1.0)
        
        m.scale.x = 11.0
        m.scale.y = 0.1
        m.scale.z = 2.0

        m.pose.position.x = 8.5
        m.pose.position.y = 3.0
        m.pose.position.z = m.scale.z/2.0

        m.color.r = 0.0
        m.color.g = 0.5
        m.color.b = 0.5
        m.color.a = 0.9
        self.markerArray.markers.append(m);


       #Simulated Wall - Right
        m2 = Marker(type=Marker.CUBE, action=Marker.ADD)
        m2.ns = 'wall2'
        m2.header.frame_id = 'world'
        m2.lifetime = rospy.Duration(1.0)
        m2.scale.x = 11.0
        m2.scale.y = 0.1
        m2.scale.z = 2.0

        m2.pose.position.x = 8.5
        m2.pose.position.y = -8.0
        m2.pose.position.z = m.scale.z/2.0

        m2.color.r = 0.0
        m2.color.g = 0.5
        m2.color.b = 0.5
        m2.color.a =0.9
        self.markerArray.markers.append(m2)


        #Simulated Wall - Back
        m3 = Marker(type=Marker.CUBE, action = Marker.ADD)
        m3.ns = 'wall3'
        m3.header.frame_id = 'world'
        m3.lifetime = rospy.Duration(1.0)
        m3.scale.x = 0.1
        m3.scale.y = 11.0
        m3.scale.z = 2.0

        m3.pose.position.x = 14.0
        m3.pose.position.y = -2.5
        m3.pose.position.z = m.scale.z/2.0

        m3.color.r = 0.0
        m3.color.g = 0.5
        m3.color.b = 0.5
        m3.color.a = 0.9
        self.markerArray.markers.append(m3)
        

        #Simulated Space text
        m4 = Marker(type=Marker.TEXT_VIEW_FACING,action = Marker.ADD)
        m4.header.frame_id = 'world'
        m4.color.r = 1.0
        m4.color.g = 1.0
        m4.color.b = 1.0
        m4.color.a = 0.5
        

        m4.scale.z = 0.3


        m4.pose.position.x = 14.0
        m4.pose.position.y = -2.0
        m4.pose.position.z = 1.0
        m4.text = 'SIMULATION SPACE'
        m4.lifetime = rospy.Duration(1.0)
        
        self.markerArray.markers.append(m4)
        
        #Base Area
        m5 = Marker(type=Marker.CUBE, action=Marker.ADD)
        m5.ns = 'base_comm'
        m5.header.frame_id = 'world'
        m5.lifetime = rospy.Duration(1.0)
        m5.scale.x = 11.0
        m5.scale.y = 0.1
        m5.scale.z = 1.0

        m5.pose.position.x = 8.5
        m5.pose.position.y = 0
        m5.pose.position.z = m.scale.z/2.0

        m5.color.r = 0.0
        m5.color.g = 0.5
        m5.color.b = 0.5
        m5.color.a = 0.3
        self.markerArray.markers.append(m5)


        #Wall separating Comm and Tasking
        m6 = Marker(type=Marker.CUBE, action=Marker.ADD)
        m6.ns = 'comm_tasking'
        m6.header.frame_id = 'world'
        m6.lifetime = rospy.Duration(1.0)
        m6.scale.x = 11.0
        m6.scale.y = 0.1
        m6.scale.z = 1.0

        m6.pose.position.x = 8.5
        m6.pose.position.y = -2.0
        m6.pose.position.z = m.scale.z/2.0

        m6.color.r = 0.0
        m6.color.g = 0.5
        m6.color.b = 0.5
        m6.color.a = 0.3
        self.markerArray.markers.append(m6)


        #Base Space text
        m7 = Marker(type=Marker.TEXT_VIEW_FACING,action = Marker.ADD)
        m7.header.frame_id = 'world'
        m7.ns = 'base_text'
        m7.color.r = 1.0
        m7.color.g = 1.0
        m7.color.b = 1.0
        m7.color.a = 0.5
        

        m7.scale.z = 0.3


        m7.pose.position.x = 8.5
        m7.pose.position.y = 2.0
        m7.pose.position.z = 1.5
        m7.text = 'Base Area'
        m7.lifetime = rospy.Duration(1.0)
        self.markerArray.markers.append(m7)
        
        #Comm Space text
        m8 = Marker(type=Marker.TEXT_VIEW_FACING,action = Marker.ADD)
        m8.header.frame_id = 'world'
        m8.ns = 'comm_text'
        m8.color.r = 1.0
        m8.color.g = 1.0
        m8.color.b = 1.0
        m8.color.a = 0.5
        

        m8.scale.z = 0.3


        m8.pose.position.x = 8.5
        m8.pose.position.y = -1.0
        m8.pose.position.z = 1.5
        m8.text = 'Comm Area'
        m8.lifetime = rospy.Duration(1.0)
        self.markerArray.markers.append(m8)
        
        #Tasking Space text
        m9 = Marker(type=Marker.TEXT_VIEW_FACING,action = Marker.ADD)
        m9.header.frame_id = 'world'
        m9.ns = 'tasking_text'
        m9.id = 0
        m9.color.r = 1.0
        m9.color.g = 1.0
        m9.color.b = 1.0
        m9.color.a = 0.5
        
        m9.pose.position.x = 8.5
        m9.pose.position.y = -5.0
        m9.pose.position.z = 1.5
        m9.scale.z = 0.3
        m9.lifetime = rospy.Duration(1.0)
        m9.text = 'Tasking Area'

        self.markerArray.markers.append(m9)
        


    def drawEnvironment(self,event):
        self.pub.publish(self.markerArray)
        
        
if __name__ == '__main__':
    rospy.init_node('SimulationEnvironment',anonymous = True)
    try:
        env = RavenBroadcast()
    except rospy.ROSInterruptException: pass
    rospy.spin()
    
       
            
