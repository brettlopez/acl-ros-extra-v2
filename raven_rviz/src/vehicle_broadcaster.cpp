/*
 * vehicle_broadcaster.cpp
 *
 *  Created on: Sep 26, 2013
 *      Author: Mark Cutler
 *              Email:  markjcutler@gmail.com
 *
 *      Simple utility for packaging up pose data from vehicles
 *      and sending it for visualization to rviz.
 */

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Pose.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <iostream>
#include <boost/algorithm/string.hpp>
#include <map>

// local includes
#include "acl_msgs/VehicleList.h"
#include "agent_marker.h"
#include "waypoint.h"

struct Agent {
    AgentMarker marker;
    Waypoint waypoint;
    Agent(std::string name, int id,
          interactive_markers::InteractiveMarkerServer* server)
        : marker(name, id)
        , waypoint(name, server)
    {
    }
};

// global vars
// TODO: not great practice, could make these member variables of a class
// instead
std::map<std::string, Agent*> agentMap;
unsigned int markerID;
interactive_markers::InteractiveMarkerServer* server;
ros::Publisher marker_pub;

// subscribe to the vicon data stream to know which vehicles are being broadcast
void vehicleListCB(const acl_msgs::VehicleList& msg)
{
    // loop over the list of current vehicles and add any needed ones to the
    // agent marker map
    for (unsigned int i = 0; i < msg.vehicle_names.size(); i++) {
        std::string name = msg.vehicle_names[i];
        if (agentMap.find(name) == agentMap.end()) {
            // not found -- add a new agent to the map
            agentMap.insert(
                std::make_pair(name, new Agent(name, markerID++, server)));
            // note: no real need to keep track of decrementing markerID since
            // it can be arbitrarily large (up to 32bit int)
        }
    }

    // loop over the current keys in the agent marker map -- delete any that
    // are no longer in the list of current vehicles
    std::map<std::string, Agent*>::iterator iter;
    for (iter = agentMap.begin(); iter != agentMap.end(); iter++) {
        std::string name = iter->first;
        if (std::find(msg.vehicle_names.begin(), msg.vehicle_names.end(), name)
            == msg.vehicle_names.end()) {
            // this key is no longer in the list of current vehicles
            // need to delete it
            ROS_INFO_STREAM("Deleting agent marker for " << name);
            iter->second = NULL;
            delete (iter->second); // free allocated memory
            agentMap.erase(iter);
        }
    }
}

void main_loop(const ros::TimerEvent&)
{
    // Get all of the markers that are ready to be published
    visualization_msgs::MarkerArray
        markerArray; // array for sending RAVEN marker messages
    std::map<std::string, Agent*>::iterator iter;
    for (iter = agentMap.begin(); iter != agentMap.end(); iter++) {
        AgentMarker am = iter->second->marker;
        if (am.shouldBroadcast()) {
            markerArray.markers.push_back(am.getMarker());
            markerArray.markers.push_back(am.getTextMarker());
        }
    }

    // Publish array
    marker_pub.publish(markerArray);
}

int main(int argc, char** argv)
{

    ros::init(argc, argv, "RVIZ_vehicle_broadcaster");
    ROS_INFO("Starting RVIZ_vehicle_broadcaster...");

    ros::NodeHandle nh;
    ros::Subscriber vd = nh.subscribe("/current_vehicles", 1, vehicleListCB);
    marker_pub
        = nh.advertise<visualization_msgs::MarkerArray>("RAVEN_vehicles", 1);

    markerID = 0;
    server = new interactive_markers::InteractiveMarkerServer("rviz", "", true);

    ros::Timer timer = nh.createTimer(ros::Duration(1 / 20.0), main_loop);

    // start asynchronous spinner
    ros::AsyncSpinner spinner(4); // Use 4 threads
    spinner.start();
    ros::waitForShutdown();

    return 0;
};
