#!/usr/bin/env python
# cartesian joystick waypoint control for quadrotor
import roslib
#roslib.load_manifest('quad_control')
import rospy
import copy
import math
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PoseStamped, Pose

# local imports
from acl_msgs.msg import QuadGoal, State
import utils
import numpy as np

NOT_FLYING = 0
FLYING = 1
LANDING = 2
TAKE = 3

TAKEOFF = 1
DISABLE = 2
RESET_INTEGRATORS = 4
ATTITUDE = 5

LEFT_X = 0
LEFT_Y = 1
RIGHT_X = 3
RIGHT_Y = 4

A = 0
B = 1
X = 2
Y = 3
RB = 5
BACK = 6
START = 7

CONTROL_DT = 0.01
MAX_ACCEL_XY = 2.0
MAX_ACCEL_Z = 0.8

TAKEOFF_ALT = 0.5


class QuadJoy:

    def __init__(self):
        self.status = NOT_FLYING
        self.transmitting = True
        self.wpType = DISABLE

        self.goal = QuadGoal()
        self.goal.xy_mode = self.goal.MODE_POS
        self.goal.z_mode = self.goal.MODE_POS
        # self.goal.waypointType = DISABLE
        self.recent_goal = QuadGoal()
        self.pose = Pose()
        self.pubGoal = rospy.Publisher('goal', QuadGoal, queue_size=1)

        self.demo_mode = rospy.get_param('~demo', False)
        self.att_thrust_mode = rospy.get_param('~joyAttitude', False)

        self.spinup_time = rospy.get_param('cntrl/spinup_time',2)

        self.xmax = rospy.get_param('~x_max',0.0)
        self.xmin = rospy.get_param('~x_min',-8.0)
        self.ymax = rospy.get_param('~y_max',3.0)
        self.ymin = rospy.get_param('~y_min',-3.0)
        self.zmax = rospy.get_param('~z_max',2.0)
        self.zmin = rospy.get_param('~z_min',0.0)

        self.alt = TAKEOFF_ALT

    def stateCB(self, data):
        self.pose.position.x = data.pos.x
        self.pose.position.y = data.pos.y
        self.pose.position.z = data.pos.z
        self.pose.orientation = data.quat

    def goalCB(self, data):
        self.recent_goal = data

    def sendGoal(self):
        # self.goal.waypointType = self.wpType
        if self.wpType == DISABLE:
            self.goal.cut_power = True
        else:
            self.goal.cut_power = False
        self.goal.header.stamp = rospy.get_rostime()

        self.pubGoal.publish(self.goal)

    def joyCB(self, data):

        if self.status == NOT_FLYING:
            self.goal.yaw = utils.quat2yaw(self.pose.orientation)
        
        # takeoff
        if data.buttons[A] and self.status == NOT_FLYING:
            self.t0 = rospy.get_time()
            self.status = TAKE
            self.wpType = TAKEOFF
            rospy.loginfo("Waiting for spin up")


            # set initial goal to current pose
            self.goal.pos = copy.copy(self.pose.position)
            self.goal.vel.x = self.goal.vel.y = self.goal.vel.z = 0
            self.goal.yaw = utils.quat2yaw(self.pose.orientation)
            self.alt = TAKEOFF_ALT + self.goal.pos.z
            # if self.demo_mode:

        # emergency disable
        elif data.buttons[B] and self.status != NOT_FLYING:
            rospy.loginfo("Killing")
            self.status = NOT_FLYING
            self.wpType = DISABLE

        # landing
        elif data.buttons[X] and self.status == FLYING:
            self.status = LANDING
            # self.wpType = LAND
            self.goal.vel.x = 0
            self.goal.vel.y = 0
            self.goal.vel.z = 0
            self.goal.dyaw = 0

        elif self.status == FLYING:
            self.wpType = 0

            # get velocities from joystick
            dx = data.axes[RIGHT_Y] * 0.02 / CONTROL_DT
            dy = data.axes[RIGHT_X] * 0.02 / CONTROL_DT
            dz = data.axes[LEFT_Y] * 0.005 / CONTROL_DT
            dyaw = data.axes[LEFT_X] * 0.02 / CONTROL_DT

            # rate limit velocities
            dx = utils.rateLimit(dx, self.goal.vel.x, -MAX_ACCEL_XY,
                                 MAX_ACCEL_XY, CONTROL_DT)
            dy = utils.rateLimit(dy, self.goal.vel.y, -MAX_ACCEL_XY,
                                 MAX_ACCEL_XY, CONTROL_DT)
            dz = utils.rateLimit(dz, self.goal.vel.z, -MAX_ACCEL_Z,
                                 MAX_ACCEL_Z, CONTROL_DT)

            # saturate to room bounds
            gx = self.goal.pos.x
            gy = self.goal.pos.y
            gz = self.goal.pos.z
            xsat = [0]
            ysat = [0]
            zsat = [0]
            self.goal.pos.x = utils.saturate(gx + dx * CONTROL_DT,
                                             max(self.xmax, gx), min(self.xmin, gx),
                                             xsat)
            self.goal.pos.y = utils.saturate(gy + dy * CONTROL_DT,
                                             max(self.ymax, gy), min(self.ymin, gy),
                                             ysat)
            self.goal.pos.z = utils.saturate(gz + dz * CONTROL_DT,
                                             max(self.zmax, gz), min(self.zmin, gz),
                                             zsat)
            # Set velocities to zero if positions are saturated
            if (xsat[0] == -1 or xsat[0] == 1):
                dx = utils.rateLimit(0.0, self.goal.vel.x,
                                     -MAX_ACCEL_XY, MAX_ACCEL_XY,
                                     CONTROL_DT)
            if (ysat[0] == -1 or ysat[0] == 1):
                dy = utils.rateLimit(0.0, self.goal.vel.y,
                                     -MAX_ACCEL_XY, MAX_ACCEL_XY,
                                     CONTROL_DT)
            if (zsat[0] == -1 or zsat[0] == 1):
                dz = utils.rateLimit(0.0, self.goal.vel.z,
                                     -MAX_ACCEL_Z, MAX_ACCEL_Z,
                                     CONTROL_DT)

            # set velocities
            self.goal.vel.x = dx
            self.goal.vel.y = dy
            self.goal.vel.z = dz

            self.goal.yaw = utils.wrap(self.goal.yaw + dyaw * CONTROL_DT)
            self.goal.dyaw = dyaw
            # self.goal.heading = 0
            # self.goal.dheading = 0

        if self.status == LANDING:
            if self.pose.position.z > 0.4:
                # fast landing
                self.goal.pos.z = utils.saturate(self.goal.pos.z - 0.0035,
                                                 2.0, -0.1)
            else:
                # slow landing
                self.goal.pos.z = utils.saturate(self.goal.pos.z - 0.001,
                                                 2.0, -0.1)
            if self.goal.pos.z == -0.1:
                self.status = NOT_FLYING
                self.wpType = DISABLE
                rospy.loginfo("Landed!")


        if self.status == TAKE:

            if (rospy.get_time()-self.t0 > self.spinup_time):
                self.goal.pos.z = utils.saturate(self.goal.pos.z+0.0035, self.alt,-0.1)
                if (np.abs(self.alt-self.pose.position.z) < 0.1 and self.goal.pos.z==self.alt):
                    self.status = FLYING
                    rospy.loginfo("Takeoff complete!")


        # stop transmitting data from joystick
        if data.buttons[BACK] and self.transmitting:
            self.transmitting = False
            self.goal.vel.x = 0
            self.goal.vel.y = 0
            self.goal.vel.z = 0
            self.goal.dyaw = 0
            for i in range(3):
                self.sendGoal()

        # start transmitting data from joystick
        if data.buttons[START] and not self.transmitting:
            self.transmitting = True
            self.goal = self.recent_goal

        # send thrust and attitude information directly to quad
        # goal.pos.x = roll
        # goal.pos.y = pitch
        # goal.vel.x = roll_rate (p)
        # goal.vel.y = pitch_rate (q)
        # goal.accel.x = throttle
        if self.att_thrust_mode and self.status == FLYING:
            self.wpType = ATTITUDE

            maxAng = math.pi / 4.0*1.2   # max commanded angle in
            rollCmd = -data.axes[RIGHT_X] * maxAng  # right_x
            pitchCmd = data.axes[RIGHT_Y] * maxAng  # right_y
            self.goal.dyaw = data.axes[LEFT_X] * .005  # left
            self.goal.yaw += self.goal.dyaw
            self.goal.pos.x = rollCmd
            self.goal.pos.y = pitchCmd
            # zero the commanded rates during joystick flying
            self.goal.vel.x = 0.0
            self.goal.vel.y = 0.0

            # get throttle
            self.goal.accel.x += data.axes[LEFT_Y]/1000
            self.goal.accel.x = utils.saturate(self.goal.accel.x ,1,0)

        if self.transmitting:
            self.sendGoal()
            

def startNode():
    c = QuadJoy()
    rospy.Subscriber("joy", Joy, c.joyCB)
    rospy.Subscriber("state", State, c.stateCB)
    rospy.Subscriber("goal", QuadGoal, c.goalCB)
    rospy.spin()

if __name__ == '__main__':

    ns = rospy.get_namespace()
    try:
        rospy.init_node('joy')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is tyipcally accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=mQ01 $ rosrun quad_control joy.py")
        else:
            print "Starting joystick teleop node for: " + ns
            startNode()
    except rospy.ROSInterruptException:
        pass
