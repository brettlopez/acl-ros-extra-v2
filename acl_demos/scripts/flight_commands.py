#!/usr/bin/env python
# cartesian joystick waypoint control for quadrotor
import roslib
#roslib.load_manifest('quad_control')
import rospy
import copy
import math
import numpy as np
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PoseStamped, PointStamped, Pose
from acl_msgs.msg import QuadWaypoint, ViconState

# local imports
from acl_msgs.msg import QuadFlightEvent

NOT_FLYING = 0
FLYING = 2
LANDING = 3

TAKEOFF = 1
DISABLE = 2
RESET_INTEGRATORS = 4
ATTITUDE = 5

LEFT_X = 0
LEFT_Y = 1
RIGHT_X = 3
RIGHT_Y = 4

A = 0
B = 1
X = 2
Y = 3
RB = 5
BACK = 6
START = 7
CENTER = 8

CONTROL_DT = 0.1



class FSM:
    def __init__(self):
        self.status = NOT_FLYING
        self.pubGoal = rospy.Publisher("global_goal", QuadWaypoint, queue_size=1)
        self.pubGoalVis = rospy.Publisher("global_goal_vis", PointStamped, queue_size=1)
        self.goal = QuadWaypoint()
        self.goal_vis = PointStamped()
        self.pubGoalVis = rospy.Publisher("global_goal_vis", PointStamped, queue_size=1)
        self.goal_vis = PointStamped()

        self.goal.point.x = 0.05
        self.goal.point.y = 0
        self.goal.point.z = 1

        self.has_pose = False
        self.goal_counter = 0

        # self.wp = np.array([[0,0,1],[0,0,1]])
        self.I = 0
        self.radius = 1
        self.alt = 1.0 #self.wp[0,2]

        self.refresh = False

        rospy.Timer(rospy.Duration(CONTROL_DT),self.goalTimer)

    def sendEvent(self):
        self.flightevent.header.stamp = rospy.get_rostime()
        self.pubEvent.publish(self.flightevent)

    def goalTimer(self,e):

        # self.check_goal()

        self.goal.header.stamp =rospy.get_rostime()
        self.goal.header.frame_id = "world"

        # goal = np.array([self.x_wp[self.goal_counter],self.y_wp[self.goal_counter],self.z_wp[self.goal_counter]])

        # if self.has_pose:
        #     d = np.linalg.norm(goal-self.pose)
        #     if d < 4 and self.goal_counter < len(self.x_wp)-1:
        #         self.goal_counter = self.goal_counter+1

        # self.goal.point.x = self.wp[self.I,0]   
        # self.goal.point.y = self.wp[self.I,1]   
        # self.goal.point.z = self.wp[self.I,2]   


        self.pubGoal.publish(self.goal)

        self.goal_vis.header.stamp =rospy.get_rostime()
        self.goal_vis.header.frame_id = "world"
        self.goal_vis.point = self.goal.point
        self.pubGoalVis.publish(self.goal_vis)

    def check_goal(self):
        # if self.status == NOT_FLYING and self.has_pose:
        if self.has_pose:
            d = np.linalg.norm(self.pose[0:2]-self.wp[self.I,0:2])
            if d < self.radius:
                if self.I < len(self.wp)-1:
                    self.I+=1
                    self.goal.heading = 0
                else:
                    self.I=0
                    self.goal.heading = np.pi


    def viconCB(self,data):
        if not self.has_pose:
            self.has_pose = True
        self.pose = np.array([data.pose.position.x,data.pose.position.y,data.pose.position.z])


    def joyCB(self, data):    
        # takeoff
        if data.buttons[A] and self.status == NOT_FLYING:
            self.status = FLYING

        # emergency disable
        elif data.buttons[B] and self.status != NOT_FLYING:
            self.status = NOT_FLYING

        # landing
        elif data.buttons[X] and self.status == FLYING:
            self.status = NOT_FLYING
                  

    def goalCB(self,data):
        self.goal.point.x = data.pose.position.x
        self.goal.point.y = data.pose.position.y
        self.goal.point.z = self.alt
        print self.goal.point


def startNode():
    c = FSM()
    rospy.Subscriber("~/joy",Joy,c.joyCB)
    rospy.Subscriber("~/move_base_simple/goal",PoseStamped,c.goalCB)
    rospy.Subscriber("vicon",ViconState,c.viconCB)
    rospy.spin()

if __name__ == '__main__':

    ns = rospy.get_namespace()
    try:
        rospy.init_node('fsm')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
        else:
            print "Starting fsm node for: " + ns
            startNode()
    except rospy.ROSInterruptException:
        pass
