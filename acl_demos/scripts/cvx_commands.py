#!/usr/bin/env python
import roslib
import rospy
import copy
import math
import numpy as np
from geometry_msgs.msg import PoseStamped, PointStamped, Vector3
from acl_msgs.msg import QuadFlightMode, QuadGoal, TermGoal

from visualization_msgs.msg import Marker, MarkerArray 

class FSM:

    def __init__(self):

        self.vicon_room = True

        self.xmax = rospy.get_param('~x_max',0.0)
        self.xmin = rospy.get_param('~x_min',-8.0)
        self.ymax = rospy.get_param('~y_max',3.0)
        self.ymin = rospy.get_param('~y_min',-3.0)
        self.zmax = rospy.get_param('~z_max',2.0)
        self.zmin = rospy.get_param('~z_min',0.0)

        self.alt = rospy.get_param('~z_alt',1.5)


        if self.xmax>0.0:
           self.vicon_room = False

        self.pubGoal     = rospy.Publisher("global_goal", PointStamped, queue_size=1, latch=True)
        self.pubTermGoal = rospy.Publisher("term_goal", TermGoal, queue_size=1, latch=True)
        self.pubStart    = rospy.Publisher("global_start", PointStamped, queue_size=1, latch=True)
        self.pubMode     = rospy.Publisher("flightmode",QuadFlightMode,queue_size=1,latch=True)
        self.pubMarker   = rospy.Publisher("/highbay", MarkerArray, queue_size=1, latch=True)

        self.term_goal = TermGoal()
        self.goal = PointStamped()
        self.start = PointStamped()
        self.mode = QuadFlightMode()
        self.vel = Vector3()

        self.term_goal.header.stamp =rospy.get_rostime()
        self.term_goal.header.frame_id = "world"
        self.term_goal.pos.x = -4.
        self.term_goal.pos.y = 0.01
        self.term_goal.pos.z = self.alt
        self.term_goal.vel.x = 0
        self.term_goal.vel.y = 0
        self.term_goal.vel.z = 0

        self.start.header.stamp =rospy.get_rostime()
        self.start.header.frame_id = "world"
        self.start.point.x = -4.
        self.start.point.y = 0
        self.start.point.z = 0

        self.goal.header.stamp =rospy.get_rostime()
        self.goal.header.frame_id = "world"
        self.goal.point.x = -4.
        self.goal.point.y = 0.01
        self.goal.point.z = self.alt

        for i in range(3):
            self.pubTermGoal.publish(self.term_goal)
            self.pubStart.publish(self.start)
            self.pubGoal.publish(self.goal)
            self.genEnvironment()
            rospy.sleep(1)            

    def modeCB(self, data):    
        self.mode = data
        # landing
        if data.mode == data.LAND:
            vnorm = np.sqrt(self.vel.x**2 + self.vel.y**2 + self.vel.z**2)
            if vnorm == 0:
                self.mode.header.stamp = rospy.get_rostime()
                self.pubMode.publish(self.mode)
                
                self.goal.point.z = 0.1

                self.goal.header.stamp =rospy.get_rostime()
                self.goal.header.frame_id = "world"
                self.pubGoal.publish(self.goal)

                self.start.header.stamp =rospy.get_rostime()
                self.start.header.frame_id = "world"
                self.pubStart.publish(self.start)
        else:
            self.pubMode.publish(self.mode)
                  

    def goalCB(self,data):
        if (data.pose.position.x < self.xmax and data.pose.position.x > self.xmin 
            and data.pose.position.y < self.ymax and data.pose.position.y > self.ymin):
            self.goal.point.x = data.pose.position.x
            self.goal.point.y = data.pose.position.y
            self.goal.point.z = self.alt
            print self.goal.point

            self.term_goal.pos.x = data.pose.position.x
            self.term_goal.pos.y = data.pose.position.y
            self.term_goal.pos.z = self.alt

            self.term_goal.header.stamp =rospy.get_rostime()
            self.term_goal.header.frame_id = "world"
            self.pubTermGoal.publish(self.term_goal)

            self.goal.header.stamp =rospy.get_rostime()
            self.goal.header.frame_id = "world"
            self.pubGoal.publish(self.goal)

            self.start.header.stamp =rospy.get_rostime()
            self.start.header.frame_id = "world"
            self.pubStart.publish(self.start)

            self.genEnvironment()

    def quadgoalCB(self,data):
        self.start.point.x = data.pos.x
        self.start.point.y = data.pos.y
        self.start.point.z = data.pos.z

        self.vel = data.vel

    def genEnvironment(self):

        markerArray = MarkerArray()

        dx = self.xmax - self.xmin
        dy = self.ymax - self.ymin

        diffx = self.xmax + self.xmin 

        p = np.array([ (diffx/2,self.ymax), (diffx/2,self.ymin), (self.xmax+0.5,0), (self.xmin-0.5,0)])
        s = np.array([ (dx+1,0.1), (dx+1,0.1), (0.1,dy), (0.1,dy)])

        for i in range(len(p)):      
            marker = Marker()
            marker.id = i
            marker.header.frame_id = "/world"
            marker.type = marker.CUBE
            marker.action = marker.ADD
            marker.scale.x = s[i,0]
            marker.scale.y = s[i,1]
            marker.scale.z = 3
            marker.color.a = 1.0;
            marker.color.r = 0.0;
            marker.color.g = 1.0;
            marker.color.b = 0.0;
            marker.pose.orientation.w = 1.0
            marker.pose.position.x = p[i,0]
            marker.pose.position.y = p[i,1]
            marker.pose.position.z = 1.5

            markerArray.markers.append(marker)

        self.pubMarker.publish(markerArray)


def startNode():
    c = FSM()
    rospy.Subscriber("~/globalflightmode",QuadFlightMode,c.modeCB)
    rospy.Subscriber("~/move_base_simple/goal",PoseStamped,c.goalCB)
    rospy.Subscriber("goal",QuadGoal,c.quadgoalCB)
    rospy.spin()

if __name__ == '__main__':

    ns = rospy.get_namespace()
    try:
        rospy.init_node('fsm')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
        else:
            print "Starting FSM node for: " + ns
            startNode()
    except rospy.ROSInterruptException:
        pass
