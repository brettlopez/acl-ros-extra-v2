#!/usr/bin/env python
# cartesian joystick waypoint control for quadrotor
import roslib
#roslib.load_manifest('quad_control')
import rospy
import copy
import math
import numpy as np
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PoseStamped, PointStamped, Pose
from acl_msgs.msg import ViconState

# local imports
from acl_msgs.msg import QuadGoal
from aclpy import utils

NOT_FLYING = 0
FLYING = 2
LANDING = 3

TAKEOFF = 1
DISABLE = 2
RESET_INTEGRATORS = 4
ATTITUDE = 5

LEFT_X = 0
LEFT_Y = 1
RIGHT_X = 3
RIGHT_Y = 4

A = 0
B = 1
X = 2
Y = 3
RB = 5
BACK = 6
START = 7
CENTER = 8

CONTROL_DT = 0.01
MAX_ACCEL_XY = 2.0
MAX_ACCEL_Z = 0.8

# ROOM Bounds
XMIN = -1.5
XMAX = 1.5
YMIN = -5.0
YMAX = 1.0
ZMIN = 0.5
ZMAX = 2.0


class FSM:

    def __init__(self):
        self.status = NOT_FLYING
        self.transmitting = True
        self.wpType = DISABLE

        self.goal = QuadGoal()
        self.goal.mode = self.goal.MODE_POS
        # self.goal.waypointType = DISABLE
        self.recent_goal = QuadGoal()
        self.int_goal = PointStamped()
        self.pose = Pose()
        self.pubGoal = rospy.Publisher('goal', QuadGoal, queue_size=1)

        rospy.Timer(rospy.Duration(CONTROL_DT),self.cmdTimer)

        self.demo_mode = rospy.get_param('~demo', False)
        self.att_thrust_mode = rospy.get_param('~joyAttitude', False)

        self.spinup_time = rospy.get_param('cntrl/spinup_time')
        
        self.A = rospy.get_param('~amplitutde', np.pi/2)
        self.w = rospy.get_param('~rate', 1)

        self.alt = 1

        self.go = False

    def poseCB(self, data):
        self.pose = data.pose

    def goalCB(self, data):
        self.recent_goal = data

    def new_global_goalCB(self,data):
        self.int_goal = data


    def sendGoal(self):
        # self.goal.waypointType = self.wpType
        if self.wpType == DISABLE:
            self.goal.cut_power = True
        else:
            self.goal.cut_power = False
        self.goal.header.stamp = rospy.get_rostime()

        self.pubGoal.publish(self.goal)

    def joyCB(self, data):
        if self.status == NOT_FLYING:
            self.goal.yaw = utils.quat2yaw(self.pose.orientation)
        
        # takeoff
        if data.buttons[A] and self.status == NOT_FLYING:
            if self.status!=TAKEOFF:                
                rospy.loginfo("Waiting for spinup...")

                # set initial goal to current pose
                self.goal.pos = copy.copy(self.pose.position)
                self.goal.vel.x = self.goal.vel.y = self.goal.vel.z = 0
                self.goal.yaw = utils.quat2yaw(self.pose.orientation)

                self.wpType = TAKEOFF

                rospy.sleep(self.spinup_time)

                self.status = TAKEOFF
                rospy.loginfo("Taking Off")


            # if self.demo_mode:
            # self.goal.pos.z = 0.5

        # emergency disable
        elif data.buttons[B] and self.status != NOT_FLYING:
            rospy.loginfo("Killing")
            self.status = NOT_FLYING
            self.wpType = DISABLE
            self.go = False


        # landing
        elif data.buttons[X] and self.status == FLYING:
            rospy.loginfo("Landing")

            self.status = LANDING
            # self.wpType = LAND
            self.goal.vel.x = 0
            self.goal.vel.y = 0
            self.goal.vel.z = 0
            self.goal.dyaw = 0
            self.go = False

        elif data.buttons[CENTER] and self.status == FLYING:
            self.goal.yaw = 0
            self.goal.dyaw = 0
            rospy.loginfo("Ready")
            
        elif data.buttons[START] and self.status == FLYING:
            rospy.loginfo("Starting")
            self.go = True
            self.start_time = rospy.get_time()
      
        # # stop transmitting data from joystick
        # if data.buttons[BACK] and self.transmitting:
        #     self.transmitting = False
        #     self.goal.vel.x = 0
        #     self.goal.vel.y = 0
        #     self.goal.vel.z = 0
        #     self.goal.dyaw = 0
        #     for i in range(3):
        #         self.sendGoal()

        # # start transmitting data from joystick
        # if data.buttons[START] and not self.transmitting:
        #     self.transmitting = True
        #     self.goal = self.recent_goal
   
                    
    def cmdTimer(self,e):
        if self.go:
            t = rospy.get_time() - self.start_time

            self.goal.yaw = self.A*np.sin(self.w*t)
            self.goal.dyaw = self.A*self.w*np.cos(self.w*t)

        if self.status == LANDING:
            if self.pose.position.z > 0.4:
                # fast landing
                self.goal.pos.z = utils.saturate(self.goal.pos.z - 0.0035,
                                                 2.0, -0.1)
            else:
                # slow landing
                self.goal.pos.z = utils.saturate(self.goal.pos.z - 0.001,
                                                 2.0, -0.1)
            if self.goal.pos.z == -0.1:
                self.status = NOT_FLYING
                self.wpType = DISABLE
                rospy.loginfo("Landed!")

        if self.status == TAKEOFF:
            self.goal.pos.z = utils.saturate(self.goal.pos.z+0.0035, self.alt,-0.1)
            if (np.abs(self.alt-self.pose.position.z) < 0.1 and self.goal.pos.z==self.alt):
                self.status = FLYING
                rospy.loginfo("Takeoff complete!")

        if self.transmitting:
            self.sendGoal()



def startNode():
    c = FSM()
    rospy.Subscriber("joy", Joy, c.joyCB)
    rospy.Subscriber("vicon", ViconState, c.poseCB)
    rospy.Subscriber("goal", QuadGoal, c.goalCB)
    rospy.Subscriber("new_global_goal", PointStamped, c.new_global_goalCB)
    rospy.spin()

if __name__ == '__main__':

    ns = rospy.get_namespace()
    try:
        rospy.init_node('joy')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is tyipcally accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=mQ01 $ rosrun quad_control joy.py")
        else:
            print "Starting joystick teleop node for: " + ns
            startNode()
    except rospy.ROSInterruptException:
        pass
