/*!
 * \file body.h
 *
 * Class holding position and orientation filters for vicon objects.
 *
 * Created on: Oct 16, 2013
 * 	   Author: Mark Cutler
 *      Email: markjcutler@gmail.com
 */

#ifndef BODY_H_
#define BODY_H_

#include <ros/ros.h> // standard ros header
#include <geometry_msgs/Pose.h>  // for the position and orientation
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/PoseStamped.h>  // for the position and orientation
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/Quaternion.h>
#include <acl_msgs/ViconState.h>
#include <vicon/ViconStatus.h>
#include <eigen3/Eigen/Dense>
#include <std_msgs/Bool.h>
#include <std_msgs/Float32.h>
#include <deque>
#include <algorithm>
#include <string>
#include <tf/transform_broadcaster.h>

#include "GHKFilter.h"
#include "attitude_ekf.h"

/**
 *  This class has position and orientation filters as members.
 */
class Body
{
public:
	Body();
	~Body();

	void update(double dt, geometry_msgs::Pose meas, bool occ, unsigned int marker_count, unsigned int num_of_markers);
	void broadcastData();
	void initPublishers();
	void resetStates();
	void readParameters();

	geometry_msgs::Pose getPose() { return pose_; }
	geometry_msgs::Twist getTwist() { return twist_; }
	geometry_msgs::Vector3 getAccel() { return accel_; }
	
	bool isOccluded() { return occluded_;}
	std::string getName() { return name_; }
	void setName(std::string name_) { this->name_ = name_; }
	bool publisher_initialized_;

protected:
	void propagationUpdate(double dt);
	void measurementUpdate(double dt, geometry_msgs::Pose meas, bool occ, unsigned int marker_count, unsigned int num_of_markers);
	ros::Publisher pub_pose_;
	ros::Publisher pub_twist_;
	ros::Publisher pub_accel_;
	ros::Publisher pub_vicon_;
	ros::Publisher pub_status_;
	tf::TransformBroadcaster transform_broadcaster_;

	bool legacy_mode_;
	bool flip_coordinate_system_;
	int skip_count_;

	bool has_last_measurement_;
	geometry_msgs::Pose last_measurement_;


	int miss_marker_tolerance_;
	int att_dropout_tol_;
	int pos_dropout_tol_;
	float q_diff_threshold_;

	std::string name_; ///< Body name_ as specified in Tracker software	
	std::string topicSubname_; ///< /<veh>/<subtopic> useful for remapping
	geometry_msgs::Pose pose_; ///< Body pose_ (position + orientation)
	geometry_msgs::Twist twist_; ///< Body linear and angular velocities
	geometry_msgs::Vector3 accel_; ///< Body linear acceleration

	bool occluded_;
	// bool jumped_;
	vicon::ViconStatus status_;

	GHKFilter ghk; ///< position, velocity, acceleration filter
	AttitudeEKF ekf; ///< attitude and attitude rate filter

	geometry_msgs::Quaternion getFlipProcessedQuaternion(geometry_msgs::Quaternion q_ref,geometry_msgs::Quaternion q_in);
	double getEuclideanDistanceSq(geometry_msgs::Quaternion a, geometry_msgs::Quaternion b);
	double getEuclideanDistanceSq(geometry_msgs::Point a, geometry_msgs::Point b);
	double getAngleDifference(geometry_msgs::Quaternion a, geometry_msgs::Quaternion b);

};

#endif /* BODY_H_ */
