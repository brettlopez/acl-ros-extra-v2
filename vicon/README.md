TODO: Rewrite the whole thing.

# Setup
Using ViconSDK version 1.3, Linux64 for boost 1.46.1

After unpacking the ViconSDK .tar file navigate to the version for Linux64 using boost 1.46.1:  

cd Vicon_SDK_1.3_Linux/Linux64-boost-1.46.1

There should be several folders named with a release date and release number (e.g. 20130116_57137h). cd into the latest release folder:   

cd 20130116_57137h/Release

From that folder, copy all the *.so* files into the /usr/local/lib folder and the .h file
into the /usr/local/include folder

sudo cp *.so* /usr/local/lib
sudo cp *.h /usr/local/include

sudo ldconfig

# How to run the new vicon filters

TODO: update these

* Turn on Vicon
* Start Vicon tracker on `armstrong`
* Execute the following command on `sikorsky`, or any machine with the tracker_filter installed.
```
$ roslaunch vicon tracker.launch
```
* Note that you should _NOT_ run the `ROSTracker` on `armstrong` anymore.

# Vicon Filters

This package contains all of the current filters used for getting the raw vicon data into a usable format for control.  At a high level this consists of differentiating and smoothing the raw position and orientation measurements that come from Vicon and piping that data out over ROS.

Note: these packages all explicitly assume that the Vicon machine can be accessed at 192.168.0.9:801.

TODO: change the hardcoded ip to a param.

### tracker_filter_linux

This can be run on any linux machine that is on the same local network as the Vicon machine.  It filters the position data using a simple GHK filter and the orientation data using an EKF.  This is the main filter used by the lab.