/*!
 * \file body.cpp
 *
 * @todo Brief file description
 *
 * Created on: Oct 16, 2013
 * 	   Author: Mark Cutler
 *     Email: markjcutler@gmail.com
 * Updated on: March, 2016
 * 		 Author: Shih-Yuan Liu
 *     Email: shihyuan.liu@gmail.com
 */

#include "body.h"

Body::Body(){
	readParameters();
	resetStates();
	publisher_initialized_ = false;
}

Body::~Body(){
	// shut down the publishers
	if (legacy_mode_){
		pub_pose_.shutdown();
		pub_twist_.shutdown();
		pub_accel_.shutdown();		
	}
	pub_status_.shutdown();
	pub_vicon_.shutdown();
}

void Body::readParameters(){
	ros::param::getCached("~legacy_mode",legacy_mode_);
	ros::param::getCached("~flip_coordinate_system",flip_coordinate_system_);
	ros::param::getCached("~miss_marker_tolerance",miss_marker_tolerance_);
	ros::param::getCached("~pos_dropout_tol",pos_dropout_tol_);
	ros::param::getCached("~att_dropout_tol",att_dropout_tol_);
	ros::param::getCached("~q_diff_threshold",q_diff_threshold_);
	ros::param::getCached("~topic_subname",topicSubname_);
}


void Body::resetStates()
{
	pose_.position.x = pose_.position.y = pose_.position.z = 0.0;
	pose_.orientation.w = 1.0;
	pose_.orientation.x = pose_.orientation.y = pose_.orientation.z = 0.0;
	twist_.linear.x = twist_.linear.y = twist_.linear.z = 0.0;
	twist_.angular.x = twist_.angular.y = twist_.angular.z = 0.0;
	accel_.x = accel_.y = accel_.z = 0.0;
	has_last_measurement_ = false;
	occluded_ = true;
	skip_count_ = 0;
	// jumped_ = false;
	// publisher_initialized_ = false;
}

void Body::initPublishers()
{
	ros::NodeHandle nh;
	std::string posename = name_  + "/" + topicSubname_;
	std::string velname = name_  + "/vel";
	std::string accelname = name_  + "/accel";
	
	pub_pose_ = nh.advertise<geometry_msgs::PoseStamped>(posename, 1);

	if (legacy_mode_){	
		pub_twist_ = nh.advertise<geometry_msgs::TwistStamped>(velname, 1);
		pub_accel_ = nh.advertise<geometry_msgs::Vector3Stamped>(accelname, 1);
		ROS_INFO_STREAM( "Broadcasting data for: " << posename << ", " << velname << ", " << accelname << std::endl);
	}

	std::string status_name = name_ + "/vicon_status";
	std::string vicon_name = name_ + "/vicon/filtered";
	pub_status_ = nh.advertise<vicon::ViconStatus>(status_name, 1);
	pub_vicon_ = nh.advertise<acl_msgs::ViconState>(vicon_name, 1);
	ROS_INFO_STREAM( "Broadcasting data for: " << status_name << ", " << vicon_name << std::endl);
	publisher_initialized_ = true;
}

void Body::broadcastData()
{
	// Do nothing if publisher not initialized yet.
	if (not publisher_initialized_){
		return;
	}
	ros::Time tn = ros::Time().now();

	// === Publishe status === //
	status_.header.stamp = tn;
	// Always publish status_
	pub_status_.publish(status_);

	// Populate ROS messages
	geometry_msgs::PoseStamped p;
	geometry_msgs::TwistStamped t;
	geometry_msgs::Vector3Stamped a;

	// === Pose === //
	p.header.stamp = tn;
	p.header.frame_id = "vicon";	
	if (flip_coordinate_system_){
		p.pose.position.x = pose_.position.y; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
		p.pose.position.y = -pose_.position.x;
		p.pose.position.z = pose_.position.z;
		p.pose.orientation.w = pose_.orientation.w;
		p.pose.orientation.x = pose_.orientation.y; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
		p.pose.orientation.y = -pose_.orientation.x;
		p.pose.orientation.z = pose_.orientation.z;
	}
	else{
		p.pose.position.x = pose_.position.x; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
		p.pose.position.y = pose_.position.y;
		p.pose.position.z = pose_.position.z;
		p.pose.orientation.w = pose_.orientation.w;
		p.pose.orientation.x = pose_.orientation.x; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
		p.pose.orientation.y = pose_.orientation.y;
		p.pose.orientation.z = pose_.orientation.z;			
	}

	// === Twist === //
	t.header.stamp = tn;
	t.header.frame_id = "vicon";
	if (flip_coordinate_system_){
		t.twist.linear.x = twist_.linear.y;
		t.twist.linear.y = -twist_.linear.x;
		t.twist.linear.z = twist_.linear.z;
		t.twist.angular.x = twist_.angular.y;
		t.twist.angular.y = -twist_.angular.x;
		t.twist.angular.z = twist_.angular.z;
	}
	else{
		t.twist.linear.x = twist_.linear.x;
		t.twist.linear.y = -twist_.linear.y;
		t.twist.linear.z = twist_.linear.z;
		t.twist.angular.x = twist_.angular.x;
		t.twist.angular.y = twist_.angular.y;
		t.twist.angular.z = twist_.angular.z;			
	}

	// === Accel === //
	a.header.stamp = tn;
	a.header.frame_id = "vicon";
	if (flip_coordinate_system_){
		a.vector.x = accel_.y; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
		a.vector.y = -accel_.x;
		a.vector.z = accel_.z;
	}
	else{
		a.vector.x = accel_.x; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
		a.vector.y = accel_.y;
		a.vector.z = accel_.z;			
	}

	// Broadcast ROS data
	if (status_.status == vicon::ViconStatus::STATUS_PUB)
	{

		acl_msgs::ViconState vicon_msg;
		vicon_msg.header.stamp = tn;
		vicon_msg.header.frame_id = "vicon";
		vicon_msg.pose = p.pose;
		vicon_msg.twist = t.twist;
		vicon_msg.accel = a.vector;
		vicon_msg.has_pose = true;
		vicon_msg.has_twist = true;
		vicon_msg.has_accel = true;

		pub_vicon_.publish(vicon_msg);

		pub_pose_.publish(p);

		if (legacy_mode_){	
			pub_twist_.publish(t);
			pub_accel_.publish(a);
		}

		// Translation only
	  tf::Transform tf_trans;
	  tf_trans.setOrigin(tf::Vector3(p.pose.position.x,p.pose.position.y,p.pose.position.z));
	  tf::Quaternion q_none(0.0,0.0,0.0,1.0);
	  tf_trans.setRotation(q_none);
	  transform_broadcaster_.sendTransform(tf::StampedTransform(tf_trans, tn, "vicon", name_ + "/trans"));
	  
	  // Full transform
	  tf::Transform tf_rot;
	  tf_rot.setOrigin(tf::Vector3(0.0,0.0,0.0));
	  tf::Quaternion q(p.pose.orientation.x,p.pose.orientation.y,p.pose.orientation.z,p.pose.orientation.w);
	  tf_rot.setRotation(q);
	  transform_broadcaster_.sendTransform(tf::StampedTransform(tf_rot, tn, name_ + "/trans", name_));
	}

}

void Body::propagationUpdate(double dt)
{
		ghk.propUpdate(dt);
		ekf.propUpdate(dt);
}

void Body::update(double dt, geometry_msgs::Pose meas, bool occ, unsigned int marker_count, unsigned int marker_count_max){
	if (not occ && not publisher_initialized_){
			initPublishers();
	}
	propagationUpdate(dt);
	measurementUpdate(dt, meas, occ, marker_count, marker_count_max);
}


void Body::measurementUpdate(double dt, geometry_msgs::Pose measurement, bool occluded_flag, unsigned int marker_count, unsigned int marker_count_max)
{
	// Handle initialization
	if (not has_last_measurement_){
		last_measurement_ = measurement;
		has_last_measurement_ = true;
	}
	occluded_ = occluded_flag;	
	// Handle possible quaternion representation flip
	geometry_msgs::Quaternion orientation_flipped = getFlipProcessedQuaternion(pose_.orientation,measurement.orientation);

	// Populate status
	status_.marker_count = marker_count;
	status_.marker_count_max = marker_count_max;
	status_.q_diff = getAngleDifference(last_measurement_.orientation,orientation_flipped);
	status_.occluded = occluded_flag;
	status_.q_jumped = (status_.q_diff > q_diff_threshold_);
	status_.miss_markers = (marker_count_max - marker_count > miss_marker_tolerance_);
	status_.skip_count = skip_count_;
	if (status_.occluded || status_.q_jumped || status_.miss_markers){
		status_.status = vicon::ViconStatus::STATUS_SKIP;
		skip_count_++;
	}
	else{
		status_.status = vicon::ViconStatus::STATUS_PUB;
		skip_count_=0;
	}

	if (skip_count_ > pos_dropout_tol_){
		ghk.reset();
	}

	if (skip_count_ > att_dropout_tol_){
		ekf.reset();
	}

	// Measurement Update only when status is STATUS_PUB
	if (status_.status == vicon::ViconStatus::STATUS_PUB){
		ghk.measUpdate(dt,measurement.position);
		ekf.measUpdate(orientation_flipped);
	}

	// Extract state.
	// pose_.position = measurement.position;
	pose_.position = ghk.getPos();
	twist_.linear = ghk.getVel();
	accel_ = ghk.getAcc();
	pose_.orientation = ekf.getAtt();
	// pose_.orientation = orientation_flipped;
	twist_.angular = ekf.getRate();

	// Save the last measurement if it's not occluded
	if (not occluded_flag){
		last_measurement_ = measurement;
	}
}

geometry_msgs::Quaternion Body::getFlipProcessedQuaternion(geometry_msgs::Quaternion q_ref, geometry_msgs::Quaternion q_in)
{
	// Return a flipped(or non-flipped) version of q_in that is consistent with q_ref.
	// Compute the squared Euclidean distance between the reference quaternion and the flipped
	// and non-flipped version of the input quaternion
	geometry_msgs::Quaternion q_flip = q_in;
	q_flip.w = -q_in.w;
	q_flip.x = -q_in.x;
	q_flip.y = -q_in.y;
	q_flip.z = -q_in.z;

	double dist_sq = getEuclideanDistanceSq(q_ref,q_in);
	double dist_sq_flipped = getEuclideanDistanceSq(q_ref,q_flip);
	
	if (dist_sq_flipped < dist_sq){
		return q_flip;
	}
	else{
		return q_in;
	}
}


// Return the square of the distance between point a and b
double Body::getEuclideanDistanceSq(geometry_msgs::Point a, geometry_msgs::Point b)
{
	return (a.x - b.x)*(a.x - b.x)+ (a.y - b.y)*(a.y - b.y)+ (a.z - b.z)*(a.z - b.z);
}

double Body::getEuclideanDistanceSq(geometry_msgs::Quaternion a, geometry_msgs::Quaternion b)
{
	return 	(a.w - b.w)*(a.w - b.w) + (a.x - b.x)*(a.x - b.x) +
					(a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z);
}

double Body::getAngleDifference(geometry_msgs::Quaternion a, geometry_msgs::Quaternion b)
{
	Eigen::Quaterniond q_a(a.w, a.x, a.y, a.z);
	Eigen::Quaterniond q_b(b.w, b.x, b.y, b.z);
	return q_a.angularDistance(q_b);
}
