/*!
 * \file BTPort.hpp
 *
 * Sets up a wifly device
 *
 *  Created on: May 7, 2015
 *      Author: Mark Cutler
 *
 */

#ifndef WIFLY_H_
#define WIFLY_H_

#include <iostream>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "telnet.hpp"
#include "serialPort.hpp"

/// Maximum serial port buffer size
#define SER_BUF_SZ 5000

namespace acl
{
class wifly
{

public:
    bool set_host_ip(std::string host, int port,
            std::vector<int> expected_address);
    bool setup_wifly(std::string ssid, int device_port, int host_port,
            std::string host_ip, std::string device_ip, std::string password,
            std::string serial_port, int initial_baud_rate,
            int target_baud_rate);
    bool get_acl_ip(std::vector<int> expected_address,
            std::string &acl_raven_ip);
};
}
;

#endif
