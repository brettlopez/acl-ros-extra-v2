/*!
 * \file utils.hpp
 *
 * Simple math utility functions for the ACL
 *
 *  Created on: Mar 15, 2013
 *      Author: Mark Cutler
 *     Contact: markjcutler@gmail.com
 *
 */

#include <math.h>
#include <valarray>
#include <boost/function.hpp>

#ifndef UTILS_H_
#define UTILS_H_

// Common defines
#ifndef PI
#define PI 3.14159265359 ///< The number PI
#endif
#ifndef GRAVITY
#define GRAVITY 9.81 ///< Gravity constant
#endif

/// ACL namespace for all library functions and classes
namespace acl
{

double saturate(double val, double max, double min);
void saturate(double* val, double max, double min);
double saturate(double val, double max, double min, int& sat);
double wrapHightoZero(double val, double high);
double wrap(double ang);
void wrap(double* ang);
double unwrap(double ang);
double rateLimit(double desiredVal, double currentVal, double minRate,
        double maxRate, double dt);
double sgn(double x);
double zeroBand(double val, double vmax, double vmin);
double distance2(double x1, double y1, double x2, double y2);
double norm(double x, double y);
double norm(double x, double y, double z);
double quat2yaw(double qw, double qx, double qy, double qz);
void rotate2D(double x, double y, double theta, double& xout, double& yout);
unsigned char hibyte(short x);
unsigned char lobyte(short x);
std::valarray<double>
rk4(double t0, std::valarray<double> y0, double dt,
        boost::function<std::valarray<double>(double, std::valarray<double>)> calc_der);
}
;

#endif /* UTILS_H_ */
