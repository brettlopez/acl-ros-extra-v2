/*!
 * \file pure_pursuit.h
 *
 * Description: Implements pure pursuit controller based on this paper:
 * "Performance and Lyapunov Stability of a Nonlinear Path-Following Guidance
 * Method" by Park, Deyst, and How
 * Currently only supports 2D path following
 *
 * Created on: Jan 22, 2014
 * 	   Author: Mark Cutler
 *      Email: markjcutler@gmail.com
 */

#ifndef PURE_PURSUIT_H_
#define PURE_PURSUIT_H_

#include <iostream>
#include <vector>
#include <cmath>

// Using Point Cloud Library
#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>

// Using acl library
#include "../utils.hpp"

namespace acl
{

/**
 *  @todo Pure Pursuit Controller
 */
class PurePursuit
{
public:
    PurePursuit(std::vector<std::vector<double> > path, double L1_nom,
            double L1_gain);
    virtual ~PurePursuit();
    void updateTree(std::vector<std::vector<double> > path);
    int calcL1Control(std::vector<double> p, std::vector<double> v, double psi,
            std::vector<double>& pL1, double& v_cmd, double& r_cmd,
            double& psi_cmd, int& index_close, int& index_L1);
    int getPathLength(void)
    {
        return speed.size();
    }

private:
    double L1_nom, L1, L1_gain, vcmd_min;
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
    std::vector<double> speed;
    pcl::PointCloud<pcl::PointXYZ>::Ptr path;

    double findEta(std::vector<double> p, std::vector<double> v,
            pcl::PointXYZ pL1);
    int findL1(std::vector<double> p, int& index_closest, int& L1_index);
};

} /* end namespace acl */
#endif /* PURE_PURSUIT_H_ */
