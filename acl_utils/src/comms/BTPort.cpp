/*!
 * \file BTPort.cpp
 *
 * Opens, reads from, and writes to a Bluetooth device
 *
 *  Created on: Mar 15, 2013
 *      Author: Buddy Michini
 *
 */

#include "BTPort.hpp"

namespace acl
{
int BTPort::BTInit(std::string macaddr)
{
    int status;
    struct sockaddr_rc addr =
    { 0 };
    //	struct sockaddr_l2 addr = { 0 };
    mac = macaddr.c_str();

    // set the connection parameters (who to connect to)
    addr.rc_family = AF_BLUETOOTH;
    addr.rc_channel = (uint8_t) 1;
    str2ba(mac, &addr.rc_bdaddr);

    //	// bind socket to port 0x1001 of the first available
    //    // bluetooth adapter
    //    addr.l2_family = AF_BLUETOOTH;
    //    addr.l2_psm = htobs(0x1001);
    //    str2ba( mac, &addr.l2_bdaddr );

    // connect to server
    printf("Connecting to BT device...\n");
    for (int i = 0; i < 5; i++)
    {
        // allocate a socket
        //		s = socket(AF_BLUETOOTH, SOCK_SEQPACKET,
        // BTPROTO_L2CAP);//SOCK_STREAM, BTPROTO_RFCOMM);
        s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
        status = connect(s, (struct sockaddr*) &addr, sizeof(addr));
        if (status >= 0)
            break;
        close(s);
    }
    // Couldn't connect
    if (status < 0)
    {
        perror("Couldn't connect to BT device!");
        exit(0);
    }
    printf("Connected to BT MAC %s \n", mac);

    return 1;
}

int BTPort::BTSend(char* pkt, int len)
{
    return write(s, pkt, len);
}

uint8_t BTPort::BTReceiveByte()
{
    uint8_t in;
    recv(s, &in, 1, 0);
    return in;
}
void BTPort::BTClose()
{
    close(s);
}
}
;
