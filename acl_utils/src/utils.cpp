/*!
 * \file utils.cpp
 *
 * Simple math utility functions for the ACL
 *
 *  Created on: Mar 15, 2013
 *      Author: Mark Cutler
 *     Contact: markjcutler@gmail.com
 *
 */

#include "utils.hpp"

namespace acl
{
/**
 * Saturates the input value
 * @param val Input value
 * @param max Maximum value
 * @param min Minimum value
 * @return Saturated value
 */
double saturate(double val, double max, double min)
{
    if (val > max)
        val = max;
    else if (val < min)
        val = min;

    return val;
}

/**
 * Saturates the input value
 * @param val Input value
 * @param max Maximum value
 * @param min Minimum value
 */
void saturate(double* val, double max, double min)
{
    if (*val > max)
        *val = max;
    else if (*val < min)
        *val = min;
}

/**
 * Saturates the input  value
 * @param num Input value
 * @param max Maximum value
 * @param min Minimum value
 * @param sat 1 if satured on max side, -1 if on min side, 0 otherwise
 * @return Saturated value
 */
double saturate(double num, double max, double min, int& sat)
{
    sat = 0;
    if (num < min)
    {
        num = min;
        sat = -1;
    }
    else if (num > max)
    {
        num = max;
        sat = 1;
    }
    return num;
}

/**
 * Wrap a number larger than high back around, starting at zero
 * @param val
 * @param high
 * @return
 */
double wrapHightoZero(double val, double high)
{
    if (val >= high)
        val -= high;
    return val;
}

/**
 * wrap val between PI and -PI
 * @param val Input to be wrapped
 * @return Wrapped input
 */
double wrap(double val)
{
    while (val < -PI)
        val += 2.0 * PI;
    while (val > PI)
        val -= 2.0 * PI;

    return val;
}

/**
 * wrap val between PI and -PI
 * @param val Input to be wrapped
 */
void wrap(double* val)
{
    while (*val < -PI)
        *val += 2.0 * PI;
    while (*val > PI)
        *val -= 2.0 * PI;
}

/**
 * Unwrap an angle so it is between 0 and 2*pi
 * @param ang Input angle
 * @return Unwrapped angle
 */
double unwrap(double ang)
{
    while (ang >= 2 * PI)
        ang -= 2 * PI;
    while (ang < 0)
        ang += 2 * PI;

    return ang;
}

/**
 * Limits the rate of change of the input value
 * @param desVal Value to be rate limited
 * @param curVal Current value of that variable
 * @param minRate Minimum allowable rate
 * @param maxRate Maximum allowable rate
 * @param dt Time step
 * @return Rate limited value of desVal
 */
double rateLimit(double desVal, double curVal, double minRate, double maxRate,
        double dt)
{

    if (desVal > (curVal + maxRate * dt))
    {
        desVal = curVal + maxRate * dt;
    }
    else if (desVal < (curVal + minRate * dt))
    {
        desVal = curVal + minRate * dt;
    }

    return desVal;
}

/**
 *
 * @param val
 * @return sign of input value
 */
double sgn(double val)
{
    if (val > 0)
        return (1.0);
    else if (val < 0)
        return (-1.0);
    else
        return (0.0);
}

/**
 * Zeros out any signal between vmin and vmax
 * @param val Input signal
 * @param vmin Minimum value
 * @param vmax Maximum value
 * @return Zero-banded value
 */
double zeroBand(double val, double vmin, double vmax)
{
    if (val > vmin && val < vmax)
        val = 0;

    return val;
}

/**
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 * @return Euclidean distance
 */
double distance2(double x1, double y1, double x2, double y2)
{
    return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

/**
 *
 * @param x
 * @param y
 * @return Euclidean distance from zero (norm)
 */
double norm(double x, double y)
{
    return sqrt(x * x + y * y);
}

/**
 *
 * @param x
 * @param y
 * @param z
 * @return Euclidean distance from zero (norm)
 */
double norm(double x, double y, double z)
{
    return sqrt(x * x + y * y + z * z);
}

/**
 * Get yaw component of a quaternion
 * @param qw
 * @param qx
 * @param qy
 * @param qz
 * @return
 */
double quat2yaw(double qw, double qx, double qy, double qz)
{
    double yaw = atan2(2.0 * (qw * qz + qx * qy),
            1.0 - 2.0 * (qy * qy + qz * qz));
    return yaw;
}

void rotate2D(double x, double y, double theta, double& xout, double& yout)
{
    xout = x * cos(theta) - y * sin(theta);
    yout = x * sin(theta) + y * cos(theta);
}
/**
 * @param x Short
 * @return High byte of x
 */
unsigned char hibyte(short x)
{
    return (unsigned char) (x >> 8);
}

/**
 * @param x Short
 * @return Low byte of x
 */
unsigned char lobyte(short x)
{
    return (unsigned char) (x & 0xFF);
}

/**
 * Run a Runge-Kutta 4th Order ODE solver
 * @param t0 Initial time
 * @param y0 Initial state values
 * @param dt Time step
 * @param calc_der Boost function pointer that returns the derivatives of each
 * of the states
 * @return Integrated states
 */
std::valarray<double> rk4(double t0, std::valarray<double> y0, double dt,
        boost::function<std::valarray<double>(double, std::valarray<double>)> calc_der)
{
    std::valarray<double> k1 = dt * calc_der(t0, y0);
    std::valarray<double> k2 = dt * calc_der(t0 + 0.5 * dt, y0 + 0.5 * k1);
    std::valarray<double> k3 = dt * calc_der(t0 + 0.5 * dt, y0 + 0.5 * k2);
    std::valarray<double> k4 = dt * calc_der(t0 + dt, y0 + k3);

    std::valarray<double> y = y0 + 1.0 / 6.0 * (k1 + 2.0 * k2 + 2.0 * k3 + k4);

    return y;
}
}
;
