/*!
 * \file InvertedPendulumDynamics.cpp
 *
 * @todo Brief file description
 *
 *  Created on: June 24, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

#include "InvertedPendulumDynamics.hpp"

namespace acl
{

InvPendDynamics::InvPendDynamics()
{

    // Initialize all variables
    simTime = 0.0;

    // Set default initial parameters
    // TODO: measure these
    param.m = 0.5;
    param.l = 0.75;
    param.l_cg = 2. / 3. * param.l;
    param.k_fric = 0;
    param.J = 0.02421;
    param.a = 0;

    param.noise_level = 0 * 1.0;

    // Set default initial state
    state.theta = 0.0;
    state.dtheta = 0.0;

    force = 0.0;
    omega = 0.0;

    std::normal_distribution<double> ddtheta_noise(0.0, param.noise_level); // zero mean
}

InvPendDynamics::~InvPendDynamics()
{
    // TODO Auto-generated destructor stub
}

/**
 *
 * @return
 */
struct sInvPendState InvPendDynamics::getState(void)
{
    return this->state;
}

/**
 *
 * @return
 */
struct sInvPendParam InvPendDynamics::getParams(void)
{
    return this->param;
}

void InvPendDynamics::setParamStruct(struct sInvPendParam param)
{
    this->param = param;
}

/**
 * Initialize state and reset time to zero
 * @param initState Initial state value
 */
void InvPendDynamics::setInitialState(struct sInvPendState initState)
{
    state = initState;
    simTime = 0;
}

/**
 * Set propeller force
 * @param F Force in Newtons
 */
void InvPendDynamics::setForce(double F)
{
    force = F;
}

/**
 * Get propeller force
 * @return F Force in Newtons
 */
double InvPendDynamics::getForce(void)
{
    return force;
}

/**
 * Integrate the system dynamics one time step
 * @param dt Time step
 */
void InvPendDynamics::integrateStep(double dt)
{
    std::valarray<double> currentState(STATE_LENGTH);
    std::valarray<double> nextState(STATE_LENGTH);

    currentState[0] = state.theta;
    currentState[1] = state.dtheta;

    // run the RK4 integration step
    nextState = acl::rk4(simTime, currentState, dt,
            boost::bind(&InvPendDynamics::dynamics, this, _1, _2));
    simTime += dt;

    state.theta = nextState[0];
    state.dtheta = nextState[1];
}

/**
 * System dynamics
 * @param dt Time step
 * @param s State
 * @return Derivative of state
 */
std::valarray<double> InvPendDynamics::dynamics(double dt,
        std::valarray<double> s)
{

    double theta = s[0];
    double dtheta = s[1];

    double T_grav = GRAVITY * param.m * param.l_cg * sin(theta);
    double T_prop = force * param.l;
    double T_fric = -param.k_fric * dtheta;
    double T_damp = -param.a * dtheta * fabs(omega);
    double torque = T_grav + T_prop + T_fric + T_damp;

    // System dynamics
    double thetadot = dtheta;
    double dthetadot = torque / param.J; // + ddtheta_noise(generator);

    std::valarray<double> out(STATE_LENGTH);
    out[0] = thetadot;
    out[1] = dthetadot;

    return out;
}

} /* namespace acl */
