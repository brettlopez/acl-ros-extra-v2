#!/usr/bin/env python
import rospy
import copy
from interactive_markers.interactive_marker_server import *
from interactive_markers.menu_handler import *
from visualization_msgs.msg import *
from geometry_msgs.msg import Point, PoseStamped, Pose
from acl_msgs.msg import VehicleList, QuadWaypoint, BoolStamped
from tf import TransformListener

class Quad(object):
    def __init__(self,veh_name):
        self.pub_waypoint = rospy.Publisher(veh_name+"/waypoint",QuadWaypoint,queue_size=1)
        self.pub_fsm_ready = rospy.Publisher(veh_name+"/trigger_ready",BoolStamped,queue_size=1)
        self.waypoint = QuadWaypoint()
        self.waypoint.mode = QuadWaypoint.MODE_DISABLED
    def setWaypointPose(self,pose):
        self.waypoint.header.stamp = rospy.Time.now()
        self.waypoint.pose = pose
    def publish(self):
        # rospy.loginfo(self.waypoint)
        if not self.mode() == QuadWaypoint.MODE_DISABLED:
            self.pub_waypoint.publish(self.waypoint)
    def triggerFsm(self):
        msg = BoolStamped()
        msg.header.stamp = rospy.Time.now()
        msg.data = True
        self.pub_fsm_ready.publish(msg)

    def unregister(self):
        self.pub_waypoint.unregister()
    def enable(self):
        self.waypoint.mode = QuadWaypoint.MODE_TAKEOFF
    def disable(self):
        self.waypoint.mode = QuadWaypoint.MODE_DISABLED
    def land(self):
        self.waypoint.mode = QuadWaypoint.MODE_LAND
    def kill(self):
        self.waypoint.mode = QuadWaypoint.MODE_KILL
    def mode(self):
        return self.waypoint.mode

class ServerNode(object):
    def __init__(self):
        self.tf_listener = TransformListener()
        self.server = InteractiveMarkerServer("interactive_marker_server")
        self.quad_dict = dict()
        self.setupMenu()
        self.sub_veh_list = rospy.Subscriber("~vehicle_list",VehicleList,self.cbVehList,queue_size=1)

    def setupMenu(self):
        self.menu_handler = MenuHandler()
        self.menu_handler.insert( "Ready FSM", callback=self.processFeedback )
        self.menu_handler.insert( "Enable", callback=self.processFeedback )
        self.menu_handler.insert( "Disable", callback=self.processFeedback )
        self.menu_handler.insert( "Land Now", callback=self.processFeedback )
        self.menu_handler.insert( "Kill", callback=self.processFeedback )
        # self.sub_menu_handle = menu_handler.insert( "Submenu" )
        # self.menu_handler.insert( "First Entry", parent=self.sub_menu_handle, callback=self.processFeedback )
        # self.menu_handler.insert( "Second Entry", parent=self.sub_menu_handle, callback=self.processFeedback )
    def cbVehList(self,veh_list_msg):
        # Add vehicle
        for veh_name in veh_list_msg.vehicle_names:
            veh_type = veh_name[0:2]
            if veh_type in ["uP","DB"]:
                continue
            if veh_name not in self.quad_dict.keys():
                self.addVehicle(veh_name)

        # Remove vehilce
        for veh_name in self.quad_dict.keys():
            if veh_name not in veh_list_msg.vehicle_names:
                self.removeVehicle(veh_name)

    def addVehicle(self,veh_name):
        self.insertQuadMarker(veh_name)
        self.quad_dict[veh_name] = Quad(veh_name)
        self.server.applyChanges()

    def removeVehicle(self,veh_name):
        self.server.erase(veh_name)
        self.server.applyChanges()    
        self.quad_dict[veh_name].unregister()
        self.quad_dict.pop(veh_name)

    def processFeedback(self,msg):
        veh_name = msg.marker_name
        if msg.event_type == InteractiveMarkerFeedback.MOUSE_UP:
            pose = PoseStamped()
            pose.pose = msg.pose
            pose.header = msg.header
            pose_vicon = self.tf_listener.transformPose("vicon",pose)
            self.server.setPose(name=veh_name,pose=pose_vicon.pose,header=pose_vicon.header)
            self.server.applyChanges()
            pose_vicon.header.stamp = rospy.Time.now()
            self.quad_dict[veh_name].setWaypointPose(pose_vicon.pose)
            self.quad_dict[veh_name].publish()
            # rospy.loginfo("[feedback] MOUSE_UP %s: %s" %(msg.marker_name,pose_vicon))
        # if msg.event_type == InteractiveMarkerFeedback.POSE_UPDATE:
        #     pose = PoseStamped()
        #     pose.pose = msg.pose
        #     pose.header = msg.header
        #     pose_vicon = self.tf_listener.transformPose("vicon",pose)
        #     pose_vicon.header.stamp = rospy.Time.now()
        #     self.quad_dict[veh_name].setWaypointPose(pose_vicon.pose)
        #     self.quad_dict[veh_name].publish()

        if msg.event_type == InteractiveMarkerFeedback.MENU_SELECT:
            if msg.menu_entry_id == 1: #Ready Trigger
                self.quad_dict[veh_name].triggerFsm()
                # self.server.applyChanges()
            elif msg.menu_entry_id == 2: #Enable
                self.quad_dict[veh_name].enable()
            elif msg.menu_entry_id == 3: #Disable
                self.quad_dict[veh_name].disable()
                pose = PoseStamped()
                pose.header.frame_id = veh_name + "/trans"
                self.server.setPose(name=veh_name,pose=pose.pose,header=pose.header)
                self.server.applyChanges()                
            elif msg.menu_entry_id == 4: #LAND
                self.quad_dict[veh_name].land()
                self.quad_dict[veh_name].publish()
            elif msg.menu_entry_id == 5: #Kill
                self.quad_dict[veh_name].kill()
                self.quad_dict[veh_name].publish()
                self.quad_dict[veh_name].disable()
                pose = PoseStamped()
                pose.header.frame_id = veh_name + "/trans"
                self.server.setPose(name=veh_name,pose=pose.pose,header=pose.header)
                self.server.applyChanges()

            # rospy.loginfo("[processFeedback] MENU_SELECT %s" %(msg))

    def insertQuadMarker(self,veh_name):
        int_marker = InteractiveMarker()
        int_marker.header.frame_id = veh_name + "/trans"
        int_marker.pose.position.x = 0
        int_marker.pose.position.y = 0
        int_marker.pose.position.z = 0
        int_marker.pose.orientation.w = 1.0
        int_marker.scale = 0.5

        int_marker.name = veh_name
        int_marker.description = veh_name

        # Move Plane
        control_box =  InteractiveMarkerControl()
        control_box.always_visible = True
        control_box.interaction_mode = InteractiveMarkerControl.MOVE_PLANE
        control_box.orientation.w = 1
        control_box.orientation.y = 1
        control_box.name = "move"

        marker = Marker()
        marker.type = Marker.CUBE
        marker.scale.x = 0.2
        marker.scale.y = 0.2
        marker.scale.z = 0.2
        marker.color.r = 0.5
        marker.color.g = 0.5
        marker.color.b = 0.5
        marker.color.a = 0.25
        control_box.markers.append(marker)
        int_marker.controls.append(control_box)

        # Menu
        control_menu = InteractiveMarkerControl()
        control_menu.interaction_mode = InteractiveMarkerControl.MENU
        # control_menu.description="Options"
        control_menu.name = "menu"
        int_marker.controls.append(control_menu)
        
        # Rotate Axis
        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 1
        control.orientation.z = 0
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
        control.name = "yaw"

        # Move Axis
        int_marker.controls.append(copy.deepcopy(control))
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        control.name = "height"
        int_marker.controls.append(control)
        
        self.server.insert(int_marker, self.processFeedback)
        
        self.menu_handler.apply(self.server, veh_name)


if __name__== "__main__":
    rospy.init_node("interactive_marker_server")
    node = ServerNode()
    rospy.spin()

