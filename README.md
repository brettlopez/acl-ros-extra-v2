# README #

This repo contains all the code that DOES NOT need to be onboard your vehicle of choice. This include the Vicon tracker, simulator, and demo launch files.

### How do I get set up? ###

* This repo is automatically checked out following the instructions in the acl-system repo

### Extra dependencies for ROS kinetic ###
```
sudo apt-get install ros-kinetic-control-toolbox ros-kinetic-controller-manager ros-kinetic-transmission-interface ros-kinetic-joint-limits-interface ros-kinetic-tf2-sensor-msgs
```

### Contribution guidelines ###

* Create a new branch if you want to add a new feature to the code base
* Once the feature is complete, create a pull request for a senior student to review your work
* Use the issue tracker if a bug is found in the master branch
* The master branch is the primary branch for this repo

### Who do I talk to? ###

* Repo owner or admin
